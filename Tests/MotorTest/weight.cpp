#include "weight.h"
extern "C" {
WeightSensor sensors[] = {
	// Pin, offset, slope, enabled, last_value
	{A0, 20.48, 51.812, 1, 3},
	{9, 120.75, 40.86 , 1, 3},
	{6, 10.738, 51.812, 0, 3}, // Disabled
	{9, 138.97, 42.767, 1, 3}
};
}

#define N_SENSORS 1
#define SCALING 1.0
//#define SCALING 2.0

char debug_str[DEBUG_STR_LEN];
float weight_offset = 0.0;

float get_weight_offset(void) {
	return weight_offset;
}

void set_weight_offset(float new_offset) {
	weight_offset = new_offset;
}

void reset_array(uint16_t * arr, uint16_t arr_len);

void set_sensors(void) {
  uint8_t ii;
  for(ii = 0; ii < N_SENSORS; ii++) {
    if(!sensors[ii].enabled) continue;
    pinMode(sensors[ii].pin, INPUT);
  }
}

void setup_sensors(void) {
        pinMode(A0, INPUT);
	sensors[0].pin = 6;
	sensors[0].last_value = 0;
	sensors[0].offset = 0;
	sensors[1].pin = 9; 
	sensors[1].last_value = 0;
	sensors[1].offset = 0;
#if N_SENSORS > 2
	sensors[2].pin = 6; 
	sensors[2].last_value = 0;
	sensors[2].offset = 0;
	sensors[3].pin = 9; 
	sensors[3].last_value = 0;
	sensors[3].offset = 0;
#endif
//	set_sensors();
}

uint16_t get_average(WeightSensor * sensor) {
  uint8_t ii;
  uint16_t average = 0;
  for(ii = 0; ii < N_VALUES; ii++) {
    average += sensor->values[ii];
  }
  return (average / N_VALUES);
}

uint16_t add_new_measurement(WeightSensor * sensor) {
  if(!sensor->enabled) return 0;
  sensor->last_value++;
  sensor->last_value %= N_VALUES;
  sensor->values[sensor->last_value] = analogRead(A0);
  return sensor->values[sensor->last_value];
}

float get_measurement_kg(WeightSensor * sensor) {
  //float kg = (float) (analogRead(A0) / 1024.0) * 5.0;
  add_new_measurement(sensor);
  uint16_t analog_value = get_average(sensor);
  //float kg = (float) ((((float) analog_value) - sensor->offset) / (sensor->slope));
  //float kg = (float) analog_value;
  float kg = ((float) analogRead(A0)) * 0.0778 + 0.1605 - weight_offset;
  if(kg < 0.0) kg = 0.0;
  return kg;
}

float get_total_weight(void) {
  float weight = 0.0, total_weight = 0.0;
  uint8_t ii;
  for(ii = 0; ii < N_SENSORS; ii++) {
    if(!sensors[ii].enabled) continue;
    weight = get_measurement_kg(&sensors[ii]);
    sprintf(debug_str, "Sensor %d: ", ii);
    Serial.print(debug_str);
    Serial.println(weight);
    total_weight += weight;
  }
  total_weight *= SCALING; 
  total_weight -= weight_offset;
  if(total_weight < 0.0) {
//	  weight_offset -= total_weight;
	  total_weight = 0;
  }
  return total_weight;
}

void reset_sensors(void) {
	uint8_t ii;
	for(ii = 0; ii < N_SENSORS; ii++) {
		reset_array(sensors[ii].values, N_VALUES);
		sensors[ii].values[sensors[ii].last_value] = 0;
	}   
}

/** Resets average array */
void reset_array(uint16_t * arr, uint16_t arr_len) {
	uint8_t ii = arr_len;
	while(ii--) {
		arr[ii] = 0; 
	}
}

