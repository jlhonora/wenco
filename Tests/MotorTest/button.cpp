#include "button.h"

void start_button_interrupt(void);

void setup_buttons(void) {
  	// Start/Calibration pin (Pull-up resistors enabled)
	pinMode(START_BUTTON_PIN,INPUT);
	digitalWrite(START_BUTTON_PIN,HIGH);
    //delay(50);
	//attachInterrupt(INT0, start_button_interrupt, RISING);
    //delay(50);
	// Up/Down buttons
	pinMode(UP_BUTTON_PIN,INPUT);
	digitalWrite(UP_BUTTON_PIN,HIGH);
	pinMode(DOWN_BUTTON_PIN,INPUT);
	digitalWrite(DOWN_BUTTON_PIN,HIGH);
}

volatile uint8_t start_button_state = 0; 
uint8_t start_button_pressed(void) {
	return !digitalRead(START_BUTTON_PIN);
	//return (uint8_t) start_button_state;
}

uint8_t button_up_pressed(void) {
	return !digitalRead(UP_BUTTON_PIN);
}

uint8_t button_down_pressed(void) {
	return !digitalRead(DOWN_BUTTON_PIN);
}

// This method will be called whenever an interrupt
// associated to the start button occurs
/* void start_button_interrupt(void) {
	start_button_state = 1;
}*/
