#ifndef __STEPPER_H 
#define __STEPPER_H

#include <Arduino.h>

enum STEPPER_STATE {
   AB   = 0,
   AmB  = 1,
   AmBm = 2,
   ABm  = 3,
   NONE = 4
};

#define STEPPER_TIMER_PERIOD_US 2500

#define IN1 A5
#define IN2 A4
#define IN3 A3
#define IN4 A2
void stepper_init(void);
void stepper_stop(void);
void stepper_up(void);
void stepper_down(void);

#endif
