#include <Arduino.h>

#ifndef __MOTOR_H
#define __MOTOR_H

#include <stdint.h>
#include "defs.h"
#include "stepper.h"

// Counter-clockwise
#define MOTOR_UP 1
// Clockwise
#define MOTOR_DOWN 0
#define MOTOR_STOPPED 2

#define IN1 6
#define IN2 9

void move_motor(uint8_t direction);
void setup_motor(void);
void motor_stop(void);

void motor2_up();
void motor2_down();
void motor2_stop();
void motor2_init();

#endif // __MOTOR_H
