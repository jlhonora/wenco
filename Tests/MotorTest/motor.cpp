#include "motor.h"

static uint8_t motor_state = MOTOR_STOPPED;
// Move the motor. High is clockwise
void move_motor(uint8_t direction) { // , uint8_t speed) { // Not using speed for now
	//digitalWrite(MOTOR_DIR_PIN, direction);
	//analogWrite(MOTOR_STEP_PIN, 127);
//	delay(100);
//	digitalWrite(MOTOR_STEP_PIN, 0);
  if(direction > 0) {
    if(motor_state != MOTOR_UP) {
		motor_state = MOTOR_UP;
		motor2_up();
	}
  } else {
    if(motor_state != MOTOR_DOWN) {
		motor_state = MOTOR_DOWN;
		motor2_down();
    }
  }
}

void motor_stop(void) {
    motor_state = MOTOR_STOPPED;
	motor2_stop();
	//digitalWrite(MOTOR_STEP_PIN, 0);
}

void setup_motor(void) {
  	// Motor direction
	//pinMode(MOTOR_DIR_PIN, OUTPUT);     
//	digitalWrite(MOTOR_DIR_PIN, HIGH);
	// Motor steps
	//pinMode(MOTOR_STEP_PIN, OUTPUT);
//	digitalWrite(MOTOR_STEP_PIN, HIGH);
  motor2_init();
}

void motor2_init() {
  // put your setup code here, to run once:
  pinMode(IN1, OUTPUT);
  digitalWrite(IN1, LOW);
  pinMode(IN2, OUTPUT);
  digitalWrite(IN2, LOW);
}

void motor2_stop() {
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
}

void motor2_up() {
  motor2_stop();
  delay(100);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
}

void motor2_down() {
  motor2_stop();
  delay(100);
  digitalWrite(IN2, LOW);
  digitalWrite(IN1, HIGH);
}
