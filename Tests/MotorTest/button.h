#include <Arduino.h>
#include <stdint.h>

#ifndef __BUTTON_H
#define __BUTTON_H

#include "defs.h"

void setup_buttons(void);
uint8_t start_button_pressed(void);
uint8_t button_up_pressed(void);
uint8_t button_down_pressed(void);

#endif
