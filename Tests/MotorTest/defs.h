#ifndef _defs
#define _defs

#include <Arduino.h>
#include <stdint.h>

// Start/Calibration button
#define START_BUTTON_PIN 2
#define UP_BUTTON_PIN 8
#define DOWN_BUTTON_PIN 7

// Direction of stepper (high is clockwise)
#define MOTOR_DIR_PIN 6
// Step pin for BigEasy Driver
#define MOTOR_STEP_PIN 9

// The sensor to measure
#define SENSOR1_PIN 9

// Max weight per sensor
#define WEIGHT_KG 11.6

// Maximum samples to be averaged
#define MAX_AVG_SAMPLES 4

// Length of a general-purpose debug string
#define DEBUG_STR_LEN 50

/** This structure defines all the relevant
    elements for a sensor in general */
typedef struct Sensor {
   // The pin from which to read
   uint16_t pin;
   // The historic values for averaging
   uint16_t sample_array[MAX_AVG_SAMPLES];
   // The index in which to store a new sample
   uint8_t next_index;
   // The last measured and processed value
   uint16_t last_value;
   // The offset that is substracted to each sample
   uint16_t offset;
} Sensor_t;

float sample_as_percentage(unsigned int value, unsigned int maxValue);
void calibrate(void);
#endif // _defs
