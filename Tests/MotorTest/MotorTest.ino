#include "defs.h"
/*
	Weight sensor
*/

#include "motor.h"
#include "weight.h"
#include "button.h"
#include <LiquidCrystal.h>
// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 10);

extern volatile uint8_t start_button_state;
extern float weight_offset;

uint8_t calibrating = 0;
void print_weight_in_lcd(float value);

// Elapsed time, to regulate the weight display time
uint32_t elapsed = 0;
// Display of weight in milliseconds
uint32_t lcd_display_interval = 1000;
// The last time the weight was displayed
uint32_t last_display_time = 0;


void setup(void) {
	setup_buttons();
	setup_lcd();
	setup_sensors();
	setup_motor();
	// Init serial port at 57600 baud
	Serial.begin(57600);
	Serial.println("Comenzando");
	Serial.println("Calibrando");
	lcd.clear();
	lcd.print("Calibrando");
	float total_weight = 0.0;
	calibrating = 1;
	while(1) {
		if(button_up_pressed()) {
			Serial.println("arriba");
			move_motor(MOTOR_UP);
		} else if(button_down_pressed()) {
			Serial.println("abajo");
			move_motor(MOTOR_DOWN);
		} else {
			motor_stop();
		}
		if((elapsed = millis() - last_display_time) > lcd_display_interval) {
			total_weight = get_total_weight();
			print_weight_in_lcd(total_weight);
			last_display_time = millis();
		}
		if(start_button_pressed()) {
			if(calibrating == 0) {
				lcd.clear();	
				lcd.print("Calibrando");	
				calibrating = 1;
				set_weight_offset(0.0);
			} else {
				lcd.clear();	
				lcd.print("Midiendo");	
				set_weight_offset(total_weight);
				calibrating = 0;
				Serial.println("Midiendo");
			}
			while(start_button_pressed());
			delay(100);
		}
	}
	start_button_state = 0;
	calibrating = 1;
	lcd.clear();
	lcd.print("Midiendo");
	Serial.println("Midiendo");
	measure();
}

/** The initial value of the sensor to be used
    throughout the program */
uint16_t initialValue = 0;

void loop(void) {
	calibrating = 0;
	// Read the input on analog pin 0:
	uint16_t sensorValue = measure();
	// Print the value
	Serial.println(sensorValue);
	// Print the percentage
	float percentage = sample_as_percentage(sensorValue, initialValue);
	Serial.println(percentage);
	// Make the motor turn
	delay(3);
	analogWrite(MOTOR_STEP_PIN, 15);
	delay(1);
	delay(300);
}

/** Prints the sensors values in the LCD display */
void print_weight_in_lcd(float  weight) {
	// Print the character in the LCD
	// with a nice format
	lcd.setCursor(0,1);
	lcd.print(weight);
	lcd.print("kg  ");
	Serial.print(weight);
	Serial.println(" kg");
}

/** Reads the sensor and waits for the button to
    be pressed. When the button is pressed, it takes the
    measured value as a reference for the whole program */
float measure(void) {
	//Serial.println("Move to initial position");
	//Serial.println("Press button when done");
	float initialValue = 0;
	float percentage = 0;
	do {
		initialValue = get_total_weight();
		// Print the character in the LCD
        // with a nice format
		print_weight_in_lcd(initialValue);
		delay(300);
	} while(digitalRead(START_BUTTON_PIN) != 0 && calibrating);
    if(calibrating) {
		Serial.print("Initial value:");	
		Serial.println(initialValue);
		reset_sensors();
    }
	return initialValue;
}

/** Converts an absolute value to a percentage based on
    maxValue */
float sample_as_percentage(unsigned int value, unsigned int maxValue) {
	if(maxValue == 0) return (float) value;
	return (float) (((float)value)/((float)maxValue)) * 100.0;
}

uint8_t next_index = 0;
/** Average the samples for de-noising */
uint16_t add_sample_to_average(uint16_t new_sample, Sensor_t * s) {
	// Check for rollover
	if(s->next_index >= MAX_AVG_SAMPLES) s->next_index = 0;
	s->sample_array[s->next_index++] = new_sample;
	return average(s->sample_array, MAX_AVG_SAMPLES);
}

/** Makes a new measurement */
uint16_t get_new_measure(Sensor_t * s) {
	uint16_t value = analogRead(s->pin);
	Serial.print(value);
	Serial.print("/");
	value = substract_offset(value, s->offset);
	Serial.print(value);
	Serial.print("/");
	// Add the value to the array, average the 
	// array and store that value as the last sample
	s->last_value = add_sample_to_average(value, s);
	Serial.print(s->last_value);
	Serial.print("/");
	return s->last_value;
}

/** Substracts the calibration value to every sample. If 
    the value is lower than the calibration one then we
    set it to zero */
uint16_t substract_offset(uint16_t value, uint16_t offset) {
	return (offset > value) ? 0 : (value - offset); 
}

/** Returns the average of the array */
uint16_t average(uint16_t * arr, uint8_t arr_len) {
	uint16_t avg = 0;
	uint8_t count = arr_len;
	while(count--) {
		avg += arr[count];
	}
	avg = avg / MAX_AVG_SAMPLES;
	return avg;
}

// Different setup routines

void setup_lcd(void) {
	// set up the LCD's number of columns and rows: 
	lcd.begin(16, 2);
	// Print a message to the LCD.
	lcd.print("Starting!");
}

