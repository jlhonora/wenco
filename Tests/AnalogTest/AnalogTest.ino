void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(A5, INPUT);
  pinMode(A4, INPUT);
  pinMode(A3, INPUT);
  pinMode(A2, INPUT);
}

char string[100];
uint16_t values[4];
void loop() {
  // put your main code here, to run repeatedly: 
  values[0] = analogRead(A2);
  values[1] = analogRead(A3);
  values[2] = analogRead(A4);
  values[3] = analogRead(A5);
  sprintf(string, "%04d  %04d  %04d  %04d", values[0], values[1], values[2], values[3]);
  Serial.println(string);
  delay(500);
}
