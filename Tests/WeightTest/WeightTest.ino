#include "WeightTest.h"

extern "C" {
WeightSensor sensors[] = {
	// Pin, offset, slope, enabled, last_value
	{A2, 10.738, 51.812, 1, 3},
	{A3, 120.75, 40.86 , 1, 3},
	{A4, 10.738, 51.812, 0, 3},
	{A5, 138.97, 42.767, 1, 3}
};
}

#define N_SENSORS 4
#define SCALING 1.334
//#define SCALING 2.0

void set_sensors(void) {
  uint8_t ii;
  for(ii = 0; ii < N_SENSORS; ii++) {
    if(!sensors[ii].enabled) continue;
    pinMode(sensors[ii].pin, INPUT);
  }
}

void setup() {
  Serial.begin(57600);
  set_sensors();
}

uint16_t get_average(WeightSensor * sensor) {
  uint8_t ii;
  uint16_t average = 0;
  for(ii = 0; ii < N_VALUES; ii++) {
    average += sensor->values[ii];
  }
  return (average / N_VALUES);
}

uint16_t add_new_measurement(WeightSensor * sensor) {
  if(!sensor->enabled) return 0;
  sensor->last_value++;
  sensor->last_value %= N_VALUES;
  sensor->values[sensor->last_value] = analogRead(sensor->pin);
  return sensor->values[sensor->last_value];
}

float get_measurement_kg(WeightSensor * sensor) {
  add_new_measurement(sensor);
  uint16_t analog_value = get_average(sensor);
  float kg = (float) ((((float) analog_value) - sensor->offset) / (sensor->slope));
  if(kg < 0) kg = 0;
  return kg;
}

char debug_str[100];
void loop() {
  float weight = 0, total_weight = 0;
  uint8_t ii;
  for(ii = 0; ii < N_SENSORS; ii++) {
    if(!sensors[ii].enabled) continue;
    weight = get_measurement_kg(&sensors[ii]);
    sprintf(debug_str, "Sensor %d: ", ii);
    Serial.print(debug_str);
    Serial.println(weight);
    total_weight += weight;
  }
  weight *= SCALING; 
  Serial.print("Total: ");
  Serial.println(total_weight);
  //sprintf(debug_str, "%04d  %04d  %04d  %04d", values[0], values[1], values[2], values[3]);
  delay(500);
}
