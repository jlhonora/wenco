#include <Arduino.h>
#include "defs.h"
/*
	Displacement sensor
	Uses a linear slide potentiomenter
	to measure its displacement. A calibration is done 
	at startup.
*/

void setup(void) {
	// Motor direction
	pinMode(MOTOR_DIR_PIN, OUTPUT);     
	digitalWrite(MOTOR_DIR_PIN, HIGH);
	// Motor steps
	pinMode(MOTOR_STEP_PIN, OUTPUT);
	digitalWrite(MOTOR_STEP_PIN, LOW);
	// Calibration pin (Pull-up resistors enabled)
	pinMode(CAL_BUTTON_PIN,INPUT);
	digitalWrite(CAL_BUTTON_PIN,HIGH);
	// Init serial port at 9600 baud
	Serial.begin(9600);
	calibrate();
}

/** The initial value of the sensor to be used
    throughout the program */
unsigned int initialValue = 0;

void loop(void) {
	// Read the input on analog pin 0:
	int sensorValue = analogRead(SENSOR_PIN);
	// Print the value
	Serial.println(sensorValue);
	// Print the percentage
	sample_as_percentage(sensorValue, initialValue);
	Serial.println(sensorValue);
	// Make the motor turn
	delay(3);
	analogWrite(MOTOR_STEP_PIN, 127);
	delay(1);          
	analogWrite(MOTOR_STEP_PIN, 127);
	delay(1);   
	delay(100);
}


/** Reads the sensor and waits for the button to
    be pressed. When the button is pressed, it takes the
    measured value as a reference for the whole program */
void calibrate(void) {
	Serial.println("Move to initial position");
	Serial.println("Press button when done");
	while(digitalRead(CAL_BUTTON_PIN) != 0) {
		initialValue = analogRead(SENSOR_PIN);
		Serial.println(initialValue);
	}
	Serial.print("Initial value:");	
	Serial.println(initialValue);
}

double sample_as_percentage(unsigned int value, unsigned int maxValue) {
	if(maxValue == 0) return (double) value;
	return (double) (value/maxValue);
}
