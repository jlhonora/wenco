#include <Arduino.h>
#include <Arduino.h>
/*
	Serial Port Test
	Prints a character through the serial port every 1 s
*/

void setup(void) {
	pinMode(8, OUTPUT);     
	pinMode(9, OUTPUT);
	digitalWrite(8, HIGH);
	digitalWrite(9, LOW);
	// initialize serial communication at 9600 bits per second:
	Serial.begin(9600);
}

char c = 30;
void loop(void) {
	// read the input on analog pin 0:
	int sensorValue = analogRead(A0);
	// print out the value you read:
	Serial.println(sensorValue);
	// Get a printable character
	while(!isprint(++c));
	delay(3);
	analogWrite(9, 127);
	delay(1);          
	analogWrite(9, 127);
	delay(1);   
}
