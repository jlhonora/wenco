#ifndef _defs
#define _defs

#include <Arduino.h>
#include <stdint.h>

// Calibration button
#define CAL_BUTTON_PIN 8

// Direction of stepper (high is clockwise)
#define MOTOR_DIR_PIN 8
// Step pin for BigEasy Driver
#define MOTOR_STEP_PIN 9

// The sensor to measure
#define SENSOR_PIN A5

// Total displacement distance
#define DISTANCE_MM 50.00

double sample_as_percentage(unsigned int value, unsigned int maxValue);
void calibrate(void);
#endif // _defs
