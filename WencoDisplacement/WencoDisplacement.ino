#include "defs.h"
/*
	Displacement sensor
	Uses a linear slide potentiomenter
	to measure its displacement. A calibration is done 
	at startup.
*/

#include <LiquidCrystal.h>
// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup(void) {
        setup_motor();
        setup_buttons();
        setup_lcd();
	// Init serial port at 9600 baud
	Serial.begin(115200);
        Serial.println("Partiendo");
        lcd.clear();
        lcd.print("Mover a pos 0");
        delay(250);
	calibrate();
}

/** The initial value of the sensor to be used
    throughout the program */
uint16_t initialValue = 0;
uint16_t lastValue = 0;
uint8_t print_count = 5;
void loop(void) {
  lastValue = (uint16_t) analogRead(SENSOR_PIN);
  if(print_count++ >= 5) {
    lcd.clear();
    lcd.print("Midiendo");
    print_value((int16_t) (lastValue - initialValue));
    print_count = 0;
  }
  delay(100);
  // Check if the button has been pressed again
  // If it has then we must restart the program
  unsigned int reset_counter = 50;
  while(digitalRead(CAL_BUTTON_PIN) == 0) {
    if(reset_counter-- == 0) {
      lcd.clear();
      lcd.print("Soltar boton");
      while(digitalRead(CAL_BUTTON_PIN) == 0);
      softwareReset();
    }
    delay(5);
  }
}


/** Reads the sensor and waits for the button to
    be pressed. When the button is pressed, it takes the
    measured value as a reference for the whole program */
void calibrate(void) {
	Serial.println("Move to initial position");
	Serial.println("Press button when done");
	while(digitalRead(CAL_BUTTON_PIN) != 0) {
		initialValue = (uint16_t) analogRead(SENSOR_PIN);
                print_value((int16_t) initialValue);
		delay(100);
	}
	Serial.print("Initial value:");	
	Serial.println(initialValue);
        Serial.println("Measuring");
}

// Print the values in the LCD and serial port
// with a nice format
void print_value(int16_t value) {
      // Calculations
      double percentage = sample_as_percentage(abs(value), 1023);
      double sign = value > 0 ? 1.0 : -1.0;
      //sprintf(str, "%.2f", (double) ((percentage / 100.0) * DISTANCE_MM));
      double distance_mm = (double) ((percentage / 100.0) * DISTANCE_MM * sign);
      
      // LCD print
      lcd.setCursor(0,1);
      char str[6];
      lcd.print(distance_mm);
      lcd.print("mm  ");
      
      sprintf(str, "%04d", value);
      lcd.print(str);
      lcd.print(" ");
      
      // Serial print
      Serial.print(distance_mm);
      Serial.print("mm ");
      Serial.println(str);
}

double sample_as_percentage(unsigned int value, unsigned int maxValue) {
	if(maxValue == 0) return (double) value;
	return (double) (((double)value)/((double)maxValue)) * 100.0;
}

// Different setup routines
void setup_motor(void) {
  	// Motor direction
	pinMode(MOTOR_DIR_PIN, OUTPUT);     
	digitalWrite(MOTOR_DIR_PIN, HIGH);
	// Motor steps
	pinMode(MOTOR_STEP_PIN, OUTPUT);
	digitalWrite(MOTOR_STEP_PIN, LOW);
}

void setup_buttons(void) {
  	// Calibration pin (Pull-up resistors enabled)
	pinMode(CAL_BUTTON_PIN,INPUT);
	digitalWrite(CAL_BUTTON_PIN,HIGH);
}

void setup_lcd(void) {
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("Starting!");
}

// Restarts program from beginning but does not 
// reset the peripherals and registers
void softwareReset(void) {
  asm volatile ("  jmp 0");  
} 
