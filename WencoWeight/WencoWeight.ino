#include "defs.h"
/*
	Weight sensor
*/

#include "motor.h"
#include "weight.h"
#include "button.h"
#include <LiquidCrystal.h>
// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 10);

extern volatile uint8_t start_button_state;
extern uint16_t weight_offset;
extern uint16_t last_analog_value;

uint16_t start_button_counter = 0;

uint8_t calibrating = 0;
void print_weight_in_lcd(float value);

// Elapsed time, to regulate the weight display time
uint32_t elapsed = 0;
// Display of weight in milliseconds
uint32_t lcd_display_interval = 500;
// The last time the weight was displayed
uint32_t last_display_time = 0;

void test(void) {
	Serial.begin(57600);
	test_linear_fitting();
	test_eeprom();
	while(1);
}

void setup(void) {
    //test();
	setup_buttons();
	setup_lcd();
	setup_sensors();
	setup_motor();
	// Init serial port at 57600 baud
	Serial.begin(57600);
	calibrating = 0;
	Serial.println("Comenzando");
	lcd.clear();
    if(calibrating) {
		Serial.println("Fijar cero");
		lcd.print("Fijar cero");
	} else {
		Serial.println("Midiendo");
		lcd.print("Midiendo");
		set_weight_offset(read_weight_offset_eeprom());
	}

	Serial.print("Offset: ");
	Serial.println(weight_offset);
	
	float total_weight = 0.0;
	float max_weight = 0.0;
	while(1) {
		if(button_up_pressed()) {
			Serial.println("arriba");
			move_motor(MOTOR_UP);
		} else if(button_down_pressed()) {
			Serial.println("abajo");
			move_motor(MOTOR_DOWN);
		} else {
			motor_stop();
		}
		if((elapsed = millis() - last_display_time) > lcd_display_interval) {
			total_weight = get_total_weight();
			max_weight = total_weight > max_weight ? total_weight : max_weight;
			print_weight_in_lcd(total_weight, max_weight);
			last_display_time = millis();
		}

		while(start_button_pressed() && start_button_counter < 400) {
			start_button_counter++;
			delay(5);
		}
		if(start_button_counter > 0) {
			if(start_button_counter >= 400) {
				if(calibrating == 0) {
					set_weight_offset(0);

					lcd.clear();	
					lcd.print("Fijar cero");	
					Serial.println("Fijar cero");	

					calibrating = 1;
					max_weight = 0.0;
				} else {
					Serial.print("Guardando ");
					Serial.print(last_analog_value);
					Serial.println(" en memoria");
					write_weight_offset_eeprom(last_analog_value);
					set_weight_offset(read_weight_offset_eeprom());

					Serial.print("Offset: ");
					Serial.println(weight_offset);

					lcd.clear();	
					lcd.print("Midiendo");	
					Serial.println("Midiendo");

					calibrating = 0;
					max_weight = 0.0;
				}
			} else {
				softwareReset();
			}

			start_button_counter = 0;
			while(start_button_pressed());
			delay(100);
		}
	}
}

void loop(void) {
}

/** Prints the sensors values in the LCD display */
void print_weight_in_lcd(float  weight, float max_weight) {
	// Print the character in the LCD
	// with a nice format
	lcd.setCursor(0,1);
	lcd.print(weight);
	lcd.print(" < ");
	lcd.print(max_weight);
	lcd.print(" kg ");
	Serial.print(weight);
	Serial.println(" kg");
	Serial.print("max: ");
	Serial.print(max_weight);
	Serial.println(" kg");
}

/** Converts an absolute value to a percentage based on
    maxValue */
float sample_as_percentage(unsigned int value, unsigned int maxValue) {
	if(maxValue == 0) return (float) value;
	return (float) (((float)value)/((float)maxValue)) * 100.0;
}

uint8_t next_index = 0;
/** Average the samples for de-noising */
uint16_t add_sample_to_average(uint16_t new_sample, Sensor_t * s) {
	// Check for rollover
	if(s->next_index >= MAX_AVG_SAMPLES) s->next_index = 0;
	s->sample_array[s->next_index++] = new_sample;
	return average(s->sample_array, MAX_AVG_SAMPLES);
}

/** Returns the average of the array */
uint16_t average(uint16_t * arr, uint8_t arr_len) {
	uint16_t avg = 0;
	uint8_t count = arr_len;
	while(count--) {
		avg += arr[count];
	}
	avg = avg / MAX_AVG_SAMPLES;
	return avg;
}

// Different setup routines

void setup_lcd(void) {
	// set up the LCD's number of columns and rows: 
	lcd.begin(16, 2);
	// Print a message to the LCD.
	lcd.print("Starting!");
}

// Restarts program from beginning but does not 
// reset the peripherals and registers
void softwareReset(void) {
  asm volatile ("  jmp 0");  
} 
