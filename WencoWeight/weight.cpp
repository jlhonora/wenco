#include "weight.h"
extern "C" {
WeightSensor sensors[] = {
	// Pin, offset, slope, enabled, last_value
	{A0, 20.48, 51.812, 1, 3},
	{9, 120.75, 40.86 , 1, 3},
	{6, 10.738, 51.812, 0, 3}, // Disabled
	{9, 138.97, 42.767, 1, 3}
};
}

#include <avr/eeprom.h>

#define N_SENSORS 1
#define SCALING 1.0

char debug_str[DEBUG_STR_LEN];
uint16_t weight_offset = 0;

uint16_t get_weight_offset(void) {
	return weight_offset;
}

void set_weight_offset(uint16_t new_offset) {
	weight_offset = new_offset;
}

void reset_array(uint16_t * arr, uint16_t arr_len);

void set_sensors(void) {
  uint8_t ii;
  for(ii = 0; ii < N_SENSORS; ii++) {
    if(!sensors[ii].enabled) continue;
    pinMode(sensors[ii].pin, INPUT);
  }
}

void setup_sensors(void) {
        pinMode(A0, INPUT);
	sensors[0].pin = 6;
	sensors[0].last_value = 0;
	sensors[0].offset = 0;
	sensors[1].pin = 9; 
	sensors[1].last_value = 0;
	sensors[1].offset = 0;
#if N_SENSORS > 2
	sensors[2].pin = 6; 
	sensors[2].last_value = 0;
	sensors[2].offset = 0;
	sensors[3].pin = 9; 
	sensors[3].last_value = 0;
	sensors[3].offset = 0;
#endif
//	set_sensors();
  //weight_offset = read_weight_offset_eeprom();
}

uint16_t get_average(WeightSensor * sensor) {
  uint8_t ii;
  uint16_t average = 0;
  for(ii = 0; ii < N_VALUES; ii++) {
    average += sensor->values[ii];
  }
  return (average / N_VALUES);
}

uint16_t add_new_measurement(WeightSensor * sensor) {
  if(!sensor->enabled) return 0;
  sensor->last_value++;
  sensor->last_value %= N_VALUES;
  sensor->values[sensor->last_value] = analogRead(A0);
  return sensor->values[sensor->last_value];
}

void test_linear_fitting(void) {
  uint16_t arr[] = {8, 19, 35, 51, 68, 79, 105, 201};
  uint8_t i;
  for(i = 0; i < 8; i++) {
    Serial.print(arr[i]); Serial.print(": ");
    float kg = ((float) arr[i]) * 0.0778 + 0.1605 - weight_offset;
    Serial.println(kg);
  }
}

uint16_t last_analog_value = 0;

float get_measurement_kg(WeightSensor * sensor) {
  add_new_measurement(sensor);
  uint16_t analog_value = get_average(sensor);
  last_analog_value = analogRead(A0);
  last_analog_value = last_analog_value < weight_offset ? weight_offset : last_analog_value;
  Serial.print("Last analog: ");
  Serial.println(last_analog_value);
  float kg = ((float) (last_analog_value - weight_offset)) * 0.0779;
  if(kg < 0.0) kg = 0.0;
  return kg;
}

float get_total_weight(void) {
  float weight = 0.0, total_weight = 0.0;
  uint8_t ii;
  for(ii = 0; ii < N_SENSORS; ii++) {
    if(!sensors[ii].enabled) continue;
    weight = get_measurement_kg(&sensors[ii]);
    sprintf(debug_str, "Sensor %d: ", ii);
    Serial.print(debug_str);
    Serial.println(weight);
    total_weight += weight;
  }
  total_weight *= SCALING; 
  if(total_weight < 0.0) {
	  total_weight = 0;
  }
  return total_weight;
}

void reset_sensors(void) {
	uint8_t ii;
	for(ii = 0; ii < N_SENSORS; ii++) {
		reset_array(sensors[ii].values, N_VALUES);
		sensors[ii].values[sensors[ii].last_value] = 0;
	}   
}

/** Resets average array */
void reset_array(uint16_t * arr, uint16_t arr_len) {
	uint8_t ii = arr_len;
	while(ii--) {
		arr[ii] = 0; 
	}
}

/*

float weight_index_to_float(uint16_t value) {
   return (((float) (value)) * 0.0778 + 0.1605);
}

uint16_t weight_float_to_index(float value) {
   float result = (value - 0.1605) / 0.0778;
   return (uint16_t) result;
}*/



uint8_t test_eeprom() {
   uint16_t value = 2;
   write_weight_offset_eeprom(value);
   delay(100);
   uint16_t value_read = read_weight_offset_eeprom();
   Serial.print("Read: ");
   Serial.println(value_read);
   Serial.println(value_read == value ? "OK" : "Not OK");
   return value == value_read;
}

uint8_t eeprom_read(int address) {
	return eeprom_read_byte((unsigned char *) address);
}

void eeprom_write(int address, uint8_t value) {
	eeprom_write_byte((unsigned char *) address, value);
}

uint16_t read_weight_offset_eeprom(void) {
    uint16_t value = 0;
    uint8_t value_high = eeprom_read(10);
    uint8_t value_low = eeprom_read(11);
    value = (value_high << 8) + value_low;
    
    return value;
}

void write_weight_offset_eeprom(uint16_t weight_offset) {
	// Avoid unnecessary EEPROM writes
	if(read_weight_offset_eeprom() == weight_offset) {
		return;
	}
    uint16_t value = weight_offset;
    uint8_t value_high = (value & 0xFF00) >> 8;
    uint8_t value_low = value & 0x00FF;
    eeprom_write(10, value_high);
    eeprom_write(11, value_low);
}
