#include "TimerOne.h"
#include "stepper.h"

void change_stepper_state(STEPPER_STATE s) {
    switch((STEPPER_STATE) s) {
    case AB:
      digitalWrite(IN1,LOW);
      digitalWrite(IN2,HIGH);
      digitalWrite(IN3,HIGH);
      digitalWrite(IN4,LOW);
      break;
    case AmB:
      digitalWrite(IN1,LOW);
      digitalWrite(IN2,HIGH);
      digitalWrite(IN3,LOW);
      digitalWrite(IN4,HIGH);
      break;
    case AmBm:
      digitalWrite(IN1,HIGH);
      digitalWrite(IN2,LOW);
      digitalWrite(IN3,LOW);
      digitalWrite(IN4,HIGH);
      break;
    case ABm:
      digitalWrite(IN1,HIGH);
      digitalWrite(IN2,LOW);
      digitalWrite(IN3,HIGH);
      digitalWrite(IN4,LOW);
      break;
    case NONE:
      digitalWrite(IN1,LOW);
      digitalWrite(IN2,LOW);
      digitalWrite(IN3,LOW);
      digitalWrite(IN4,LOW);    
    default:
      // Wrong state!!
      break;
  }
}

volatile int8_t coil_state = NONE;
volatile int8_t dir = -1;
void step(void) {
  coil_state = coil_state <= 0 ? 4 : coil_state;
  coil_state += dir;
  coil_state %= 4;
  change_stepper_state((STEPPER_STATE) coil_state);
}

void stepper_init(void) {
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  Timer1.initialize(STEPPER_TIMER_PERIOD_US);
  Timer1.attachInterrupt(step);
  stepper_stop();
}


void stepper_stop() {
  change_stepper_state(NONE);
  Timer1.stop();
}

void stepper_start() {
  Timer1.initialize(STEPPER_TIMER_PERIOD_US);
  Timer1.attachInterrupt(step);
//  Timer1.start();
}

void stepper_up(void) {
  dir = 1;
  stepper_start();
}

void stepper_down(void) {
  dir = -1;
  stepper_start();
}
