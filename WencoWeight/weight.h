#include <Arduino.h>
#ifndef __WEIGHT_H
#define __WEIGHT_H
#include <stdint.h>
#include <stdlib.h>

#include "defs.h"

#define N_VALUES 4

// Struct to hold all the sensor data
typedef struct WeightSensor_t {
  uint8_t pin; // The pin from which to read
  // Every sensor has a curve of the type:
  // Weight = (AnalogValue - offset)/slope [kg]
  float offset;
  float slope;
  uint8_t enabled    : 1; // Is the sensor enabled?
  uint8_t last_value : 3; // The last position used for
                          // the values array
  uint16_t values[N_VALUES];
} WeightSensor;

float get_total_weight(void);
void reset_sensors(void);
void setup_sensors(void);
void set_weight_offset(uint16_t new_offset);
uint16_t get_weight_offset(void);
void test_linear_fitting(void);

uint16_t read_weight_offset_eeprom(void);
void write_weight_offset_eeprom(uint16_t value);
uint8_t test_eeprom(void);

#endif // __defined(__WEIGHT_H)
